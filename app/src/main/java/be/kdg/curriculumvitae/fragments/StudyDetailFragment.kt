package be.kdg.curriculumvitae.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import be.kdg.curriculumvitae.R
import be.kdg.curriculumvitae.databinding.FragmentStudyDetailBinding
import be.kdg.curriculumvitae.model.Study
import be.kdg.curriculumvitae.model.getStudies

class StudyDetailFragment : Fragment(R.layout.fragment_study_detail) {
    private lateinit var binding: FragmentStudyDetailBinding
    private var index: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStudyDetailBinding.inflate(inflater, container, false)
        updateFields();
        return binding.root
    }

    fun setStudyIndex (newIndex: Int) {
        index = newIndex
        if(this::binding.isInitialized) updateFields();
    }

    private fun updateFields() {
        val study: Study = getStudies()[index];

        binding.sdImage.setImageResource(study.imageResource);
        binding.sdTitle.text = study.school
        binding.sdVan.text = study.startYear.toString()
        binding.sdTot.text = study.endYear.toString()
        binding.sdExtraInfo.text = study.comment
    }
}