package be.kdg.curriculumvitae.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import be.kdg.curriculumvitae.R
import be.kdg.curriculumvitae.fragments.StudyDetailFragment

class StudyDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_detail)
        val studyIndex = intent.getIntExtra(STUDY_INDEX, 0);

        val fragment = supportFragmentManager.findFragmentById(R.id.detail_fragment) as StudyDetailFragment
        fragment.setStudyIndex(studyIndex)
    }
}
