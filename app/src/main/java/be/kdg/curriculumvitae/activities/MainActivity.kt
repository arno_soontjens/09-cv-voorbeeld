package be.kdg.curriculumvitae.activities

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import be.kdg.curriculumvitae.R
import be.kdg.curriculumvitae.adapters.StudyAdapter
import be.kdg.curriculumvitae.fragments.StudiesFragment
import be.kdg.curriculumvitae.fragments.StudyDetailFragment
import com.google.android.material.navigation.NavigationView


/**
 * Deze activity implementeert 'OnStudySelectedListener'!
 * Als er een study aangeklikt wordt dan zal deze klasse in actie moeten schieten.
 */

const val STUDY_INDEX = "study_index";

class MainActivity : AppCompatActivity(), StudyAdapter.OnStudySelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStudySelected(studyIndex: Int) {
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            val studyDetail = Intent(this, StudyDetailActivity::class.java);
            studyDetail.putExtra(STUDY_INDEX, studyIndex)
            startActivity(studyDetail);
        } else {
            val fragment = supportFragmentManager.findFragmentById(R.id.studies_detail_fragment) as StudyDetailFragment
            fragment.setStudyIndex(studyIndex)
        }
    }
}
